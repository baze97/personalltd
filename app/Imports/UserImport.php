<?php

namespace App\Imports;

use App\Models\user;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        return new user([
            'User' => $row['user'],
            'Client' => $row['client'],
            'Client type' => $row['client_type'],
            'Date' => $row['date'],
            'Duration' => $row['duration'],
            'Type of call' => $row['type_of_call'],
            'External Call Source' => $row['external_call_score'],
        ]);
    }
}
