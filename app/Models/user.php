<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class user extends Model
{
    use HasFactory;

    protected $table = "user";

    protected $fillable = [
        'User',
        'Client',
        'Client type',
        'Date', 
        'Duration',
        'Type of call',
        'External Call Score'
    ];

    public static function getUser()
    {
        $records = DB::table('user')->select('id', 'User', 'Client', 'Client Type', 'Date', 'Duration', 'Type of call', 'External Call Score');
        return $records;
    }
}
