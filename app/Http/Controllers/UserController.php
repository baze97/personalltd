<?php

namespace App\Http\Controllers;

use App\Models\user;
use Illuminate\Http\Request;
use Excel;
use App\Imports\UserImport;
use Facade\FlareClient\Http\Response;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\View;

class UserController extends Controller
{
    public function importForm()
    {
        return view('import-form');
    }

    public function import(Request $request)
    {
        Excel::import(new UserImport, $request->file);
        return "Record imported successfully!";
    }

    public function calls()
    {
        $users = user::paginate(50);
        return view('calls')->with('call', $users);
    }

    public function create()
    {
        return view('create');
    }
    
    public function createUser(Request $request)
    {
        // dd($request->request);
        $request->validate([
            'user' => 'required',
            'client' => 'required',
            'client_type' => 'required',
            'date' => 'required',
            'duration' => 'required',
            'type_of_call' => 'required',
            'external_call_score' => 'required',
        ]);

        $user = new user([
            'User' => $request->get('user'),
            'Client' => $request->get('client'),
            'Client type' => $request->get('client_type'),
            'Date' => $request->get('date'),
            'Duration' => $request->get('duration'),
            'Type of call' => $request->get('type_of_call'),
            'External Call Score' => $request->get('external_call_score')
        ]);

        $user->save();
        return redirect('/import-form');
    }


    public function index()
    {
        $user = user::all();
        return view('/import-form');
    }

    

    public function update(Request $request)
    {
        $user = user::findOrFail($request->id);
        return view('edit')->with('user', $user);
    }
    

    public function userUpdate(Request $request)
    {
        $user = user::findOrFail($request->id);
        $request->validate([
            'user' => 'required',
            'client' => 'required',
            'client_type' => 'required',
            'date' => 'required',
            'duration' => 'required',
            'type_of_call' => 'required',
            'external_call_score' => 'required',
        ]);

        $user->User = $request->get('user');
        $user->Client = $request->get('client');
        $user['Client Type'] = $request->get('client_type');
        $user->Date = $request->get('date');
        $user->Duration = $request->get('duration');
        $user['Type of call'] = $request->get('type_of_call');
        $user['External Call Score'] = $request->get('external_call_score');
        $user->save();
        return redirect('/import-form');
    }
    

    public function destroy(Request $request)
    {
        $user = user::findOrFail($request->id);
        $user->delete();
        return redirect('/calls');
    }
}
