<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',  [UserController::class, 'create']);

// Route::post('/', [UserController::class, 'store']);

Route::get('/import-form', [UserController::class, 'importForm']);

Route::post('/import', [UserController::class, 'import'])->name('user.import');

Route::get('/calls', [UserController::class, 'calls'])->name('user.calls');

Route::get('/create', [UserController::class, 'create'])->name('create');

Route::post('/create', [Usercontroller::class, 'createUser'])->name('createUser');

Route::get('/edit', [UserController::class, 'update'])->name('update');

Route::post('/edit', [UserController::class, 'userUpdate'])->name('userUpdate');

Route::get('/delete', [UserController::class, 'destroy'])->name('destroy');

