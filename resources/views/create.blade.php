<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal ltd</title>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
        integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
    @include ("navbar")
<div class="container">
    <form method="POST" action="{{ route('createUser')}}">
        @csrf 
        <div class="form-group">
            <label>User</label>
            <input type="text" class="form-control" name="user">
        </div>
        <div class="form-group">
            <label>Client</label>
            <input type="text" class="form-control" name="client">
        </div>
        <div class="form-group">
            <label>Client Type</label>
            <input type="text" class="form-control" name="client_type">
        </div>
        <div class="form-group">
            <labe><b>Date</b></label>
            <input type="datetime" class="form-control" name="date">
        </div>
        <div class="form-group">
            <label>Duration</label>
            <input type="number" class="form-control" name="duration">
        </div>
        <div class="form-group">
            <label>Type Of Call</label>
            <input type="text" class="form-control" name="type_of_call">
        </div>
        <div class="form-group">
            <label>External Call Score</label>
            <input type="number" class="form-control" name="external_call_score">
        </div>
        <button type="submit" class="btn btn-outline-primary">Add New User</button>
    </form>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
    integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
</script>
</html>