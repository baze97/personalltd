<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserCsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function(Blueprint $table){
            $table->id();
            $table->string('User', 255);
            $table->string('Client', 255);
            $table->string('Client Type', 255);
            $table->dateTime('Date');
            $table->integer('Duration');
            $table->string('Type of call');
            $table->integer('External Call Score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
